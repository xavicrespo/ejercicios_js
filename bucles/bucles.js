/* function inicio() {
    //bucle1();
    bucle2();
} */

function bucle1() {
    let num = prompt("Introduce un numero del 1 al 10: ");

    while (num > 10 | num < 1) {
        num = prompt("Numero incorrecto, vuelve a introducir un numero: ");
    }
    let cuadrado = num * num;
    alert("El cuadrado de " + num + " es " + cuadrado);

}

function bucle2() {
    let contador = 0;
    let total = 0;
    let numero = prompt("Introduce un numero hasta que sea 0: ");
    numero = parseInt(numero);

    while (numero > 0) {
        contador++;
        total = total + numero;
        numero = parseInt(prompt("Introduce un numero: "));
    }

    console.log("la suma es: " + total);
    console.log("El promedio es: " + total / contador);
    console.log("Número de elementos es: " + contador);
}

function bucle3() {
    let inicial = prompt("Valor inicial: ");
    let num = inicial;
    num = parseInt(num);
    let fact = num;
    do {
        fact = fact * (num - 1);
        num--;
    } while (num > 1);
    alert("El factorial de " + inicial + " es " + fact);

}

function bucle4() {
    let num = prompt("Introduce un numero: ")
    let count = num;
    let sumaPares = 0;
    let nextPares = 0;
    while (count > 0) {
        sumaPares += nextPares
        nextPares += 2; //actualitzo el nextPares
        count--;
    }
    alert("La suma de los " + num + " primeros numeros pares(incluido el 0) es: " + sumaPares)
}

function bucle5() {
    let cadena = prompt("Introduce una cadena: ");
    count = 3;
    while (count > 0) {
        password = prompt("Introduce la contra, tienes 3 intentos: ");
        if (password == null) {
            alert("fugamos");
            return;
        }
        if (password == cadena) {
            alert("Bingo! La contrasenya es:" + cadena);
            return;
        } else {
            count--;
            alert("Nope, try again (Intentos restantes: " + count + ")");
        }
    }
    alert("Acceso denegado");
}

function bucle6() {

}